<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\AdminController@index')->name('admin.index');
    Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');

    Route::get('/agent/create', 'Admin\AgentController@showCreatePage')->name('admin.agent.showCreatePage');
    Route::post('/agent/create', 'Admin\AgentController@create')->name('admin.agent.create');
    Route::get('/agent', 'Admin\AgentController@show')->name('admin.agent.show');
});

Route::prefix('agent')->group(function () {
    Route::get('/', 'Agent\AgentController@index')->name('agent.index');
    Route::get('/login','Agent\Auth\AgentLoginController@showLoginForm')->name('agent.login');
    Route::post('/login', 'Agent\Auth\AgentLoginController@login')->name('agent.login.submit');
    Route::get('/logout', 'Agent\Auth\AgentLoginController@logout')->name('agent.logout');

    Route::get('/account/create', 'Agent\AccountController@showCreatePage')->name('agent.account.showCreatePage');
    Route::post('/account/create', 'Agent\AccountController@create')->name('agent.account.create');
    Route::get('/account', 'Agent\AccountController@show')->name('agent.account.show');
//    Route::get('/salon/list','Agent\SalonController@index')->name('agent.salon.index');

});
