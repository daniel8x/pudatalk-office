<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Traits\ValidationTrait as ValidationTrait;

class AdminLoginController extends Controller
{
    use ValidationTrait;

    private $rules = [
        'email' => 'required|email',
        'password' => 'required|min:6'
    ];

    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {

        if (!$this->validator($request->all(), $this->rules)) {
            return back()->withErrors($this->validationErrors());
        };

        if (Auth::guard('admin')->attempt([
                        'email' => $request->email,
                        'password' => $request->password],
                        $request->remember))
        {
            return redirect()->intended(route('admin.index'));
        }

        return redirect()->back()->withInput($request->only('email', 'remember'))->with('fails', 'Email/password wrong, or account not activated.');
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect(route('admin.login'));
    }
}