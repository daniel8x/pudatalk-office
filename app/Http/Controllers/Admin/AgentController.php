<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataMappers\AgentMapper;
use App\DomainModels\Agent;
use App\DomainModels\Name;
use App\DomainModels\Login;
use App\DomainModels\Address;
use App\Traits\ValidationTrait as ValidationTrait;
use Auth;

class AgentController extends Controller
{
    use ValidationTrait;
    private $rules = [
        'email' => 'required|email|unique:agents',
        'password' => 'required|min:6',
        'password_confirm' => 'required|same:password'
    ];
    private $agentMapper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->agentMapper = resolve(AgentMapper::class);

    }

    /**
     * show agents table.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $agents = $this->agentMapper->getAll()->toAllArray();
        $numberOfAgent = count($agents);
        return view('admin.agent',
                ['agents' => $agents,
                'numberOfAgent' => $numberOfAgent]);

    }

    public function showCreatePage()
    {
        return view('admin.agent-create');
    }

    public function create(Request $request)
    {
        if (!$this->validator($request->all(), $this->rules)) {
            return back()->withErrors($this->validationErrors());
        };

        $request['admin_id'] = Auth::id();

        $this->agentMapper->create(
            $this->agentDTO($request->all()));

        return $this->show();
    }

    private function agentDTO($input)
    {
        if (is_array($input)) {
            return new Agent(
                '',
                $input['admin_id'],
                Name::fromAssoc($input),
                Login::fromAssoc($input),
                '',
                Address::fromAssoc($input),
                '',
                0
            );
        }
    }
}

