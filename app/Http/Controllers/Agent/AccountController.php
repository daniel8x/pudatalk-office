<?php

namespace App\Http\Controllers\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataMappers\AccountMapper;
use App\DomainModels\Account;
use App\DomainModels\Name;
use App\DomainModels\Address;
use App\DomainModels\Login;
use App\Traits\ValidationTrait as ValidationTrait;
use Auth;


class AccountController extends Controller
{
    private $accountMapper;

    use ValidationTrait;
    private $rules = [
        'email' => 'required|email|unique:accounts',
        'password' => 'required|min:6',
        'password_confirm' => 'required|same:password'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:agent');
        $this->accountMapper = resolve(AccountMapper::class);
    }

    /**
     * show agents table.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
//        $salons = $this->salon->getByAgentID(Auth::id());

        $accounts = $this->accountMapper->getByAgentID(Auth::id())->toAllArray();
        $numberOfAccount = count($accounts);
        return view('agent.account',
            ['accounts' => $accounts, 'numberOfAccount' => $numberOfAccount]);

    }

//    public function index()
//    {
//        $salonList = $this->salon->getAll();
//        return response()->json(['success' => true, 'salonList' => $salonList]);
//    }

    public function showCreatePage()
    {
        return view('agent.account-create');
    }

    public function create(Request $request)
    {
        if (!$this->validator($request->all(), $this->rules)) {
            return back()->withErrors($this->validationErrors());
        };
        $data = $request->all();
        $data['agent_id'] = Auth::id();
        $this->accountMapper->create($this->accountDTO($data));
        return $this->show();
    }

    private function accountDTO($input)
    {
        if (is_array($input)) {
            return new Account(
                '',
                $input['agent_id'],
                Name::fromAssoc($input),
                Login::fromAssoc($input),
                '',
                Address::fromAssoc($input),
                1,
                '',
                ''
            );
        }

    }
}

