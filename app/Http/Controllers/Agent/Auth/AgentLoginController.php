<?php

namespace App\Http\Controllers\Agent\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Traits\ValidationTrait as ValidationTrait;

class AgentLoginController extends Controller
{
    use ValidationTrait;

    private $rules = [
        'email' => 'required|email',
        'password' => 'required|min:6'
    ];

    public function __construct()
    {
        $this->middleware('guest:agent', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
        return view('agent.login');
    }

    public function login(Request $request)
    {

        if (!$this->validator($request->all(), $this->rules)) {
            return back()->withErrors($this->validationErrors());
        };

        if (Auth::guard('agent')->attempt([
            'email' => $request->email,
            'password' => $request->password],
            $request->remember))
        {
            return redirect()->intended(route('agent.index'));
        }

        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('agent')->logout();
        return redirect(route('agent.login'));
    }
}