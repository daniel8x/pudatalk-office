<?php

namespace App\DataMappers;

use App\Collections\AccountCollection;
use App\DomainModels\Account;
use App\Repositories\Account\AccountInterface;

class AccountMapper
{

    private $accountLoader;

    public function __construct(AccountInterface $accountLoader)
    {
        $this->accountLoader = $accountLoader;
    }

    public function create(Account $account)
    {
        return $this->accountLoader->create($account);
    }

    public function getAll(): AccountCollection
    {
        return $this->accountLoader->getAll();
    }

    public function getByAgentId($id): AccountCollection
    {
        return $this->accountLoader->getByAgentID($id);
    }
}