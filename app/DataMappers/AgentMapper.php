<?php

namespace App\DataMappers;

use App\Collections\AgentCollection;
use App\DomainModels\Agent;
use App\Repositories\Agent\AgentInterface;

class AgentMapper
{

    private $agentLoader;

    public function __construct(AgentInterface $agentLoader)
    {
        $this->agentLoader = $agentLoader;

    }

    public function create(Agent $agent)
    {
        return $this->agentLoader->create($agent);
    }

    public function getAll(): AgentCollection
    {
        return $this->agentLoader->getAll();
    }
}