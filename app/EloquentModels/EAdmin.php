<?php

namespace App\EloquentModels;

use Illuminate\Foundation\Auth\User as Authenticatable;

class EAdmin extends Authenticatable
{
    protected $table = 'admins';
    protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'middlename', 'lastname', 'email', 'password'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function agents()
    {
        return $this->hasMany('App\EloquentModels\EAgent','admin_id','id');
    }
}