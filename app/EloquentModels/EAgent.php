<?php

namespace App\EloquentModels;

use Illuminate\Foundation\Auth\User as Authenticatable;

class EAgent extends Authenticatable
{
    protected $guard = 'agent';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'agents';

    protected $fillable = [
        'admin_id','firstname', 'middlename', 'lastname', 'email', 'password', 'cellphone', 'homeaddress1', 'homeaddress2', 'city', 'state', 'zipcode', 'employee_status', 'ssn'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}