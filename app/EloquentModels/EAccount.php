<?php

namespace App\EloquentModels;

use Illuminate\Foundation\Auth\User as Authenticatable;

class EAccount extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'accounts';

    protected $fillable = [
        'agent_id','firstname', 'middlename', 'lastname', 'email', 'password', 'cellphone', 'homeaddress1', 'homeaddress2', 'city', 'state', 'zipcode', 'startdate', 'enddate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}