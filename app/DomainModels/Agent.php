<?php
namespace App\DomainModels;

class Agent {
    private $id;
    private $admin_id;
    private $name;
    private $login;
    private $cellphone;
    private $address;
    private $ssn;
    private $status;

    /**
     * Agent constructor.
     * @param $id
     * @param $admin_id
     * @param $name
     * @param $login
     * @param $address
     * @param $ssn
     * @param $status
     */
    public function __construct($id, $admin_id, Name $name, Login $login, $cellphone, Address $address, $ssn, $status)
    {
        $this->id = $id;
        $this->admin_id = $admin_id;
        $this->name = $name;
        $this->login = $login;
        $this->address = $address;
        $this->cellphone = $cellphone;
        $this->ssn = $ssn;
        $this->status = $status;
    }

    public static function fromAssoc(array $data): Agent
    {
        $id = $data['id'] ?? null;
        $admin_id = $data['admin_id'] ?? 1;
        $name = $data['name'] ?? null;
        $login = $data['login'] ?? null;
        $address = $data['address'] ?? null;
        $cellphone = $data['cellphone'] ?? null;
        $ssn = $data['ssn'] ?? null;
        $status = $data['status'] ?? 0;
        return new Agent($id, $admin_id, $name, $login, $cellphone, $address, $ssn, $status);

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @return Login
     */
    public function getLogin(): Login
    {
        return $this->login;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getSsn()
    {
        return $this->ssn;
    }

    /**
     * @return mixed
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
    * @return mixed
    */
    public function getAdminId()
    {
        return $this->admin_id;
    }



    public function toArray()
    {
        return [
            'id' => $this->id,
            'admin_id' => $this->admin_id,
            'name' => $this->name->toArray(),
            'login' => $this->login->toArray(),
            'cellphone' => $this->cellphone,
            'address' => $this->address->toArray(),
            'ssn' => $this->ssn,
            'status' => $this->status
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }





}