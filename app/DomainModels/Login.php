<?php

namespace App\DomainModels;

class Login {
    private $username;
    private $email;
    private $password;

    /**
     * Login constructor.
     * @param $id
     * @param $username
     * @param $email
     * @param $password
     */
    public function __construct($username, $email, $password)
    {
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
    }

    public static function fromAssoc(array $data): Login
    {
        $username = $data['username'] ?? '';
        $email = $data['email'] ?? '';
        $password = $data['password'] ?? '';

        return new Login($username, $email, $password);

    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }



    public function toArray()
    {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'username' => $this->username
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }




}