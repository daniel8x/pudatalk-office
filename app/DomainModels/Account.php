<?php
namespace App\DomainModels;

class Account {
    private $id;
    private $agent_id;
    private $name;
    private $login;
    private $cellphone;
    private $address;
    private $status;
    private $startdate;
    private $enddate;

    /**
     * Account constructor.
     * @param $id
     * @param $agent_id
     * @param $name
     * @param $login
     * @param $cellphone
     * @param $address
     * @param $status
     * @param $startdate
     * @param $enddate
     */
    public function __construct($id, $agent_id, Name $name, Login $login, $cellphone, Address $address, $status, $startdate, $enddate)
    {
        $this->id = $id;
        $this->agent_id = $agent_id;
        $this->name = $name;
        $this->login = $login;
        $this->cellphone = $cellphone;
        $this->address = $address;
        $this->status = $status;
        $this->startdate = $startdate;
        $this->enddate = $enddate;
    }


    public static function fromAssoc(array $data): Account
    {
        $id = $data['id'] ?? null;
        $agent_id = $data['agent_id'] ?? 1;
        $name = $data['name'] ?? null;
        $login = $data['login'] ?? null;
        $cellphone = $data['cellphone'] ?? null;
        $address = $data['address'] ?? null;
        $status = $data['status'] ?? 0;
        $startdate = $data['startdate'] ?? null;
        $enddate = $data['enddate'] ?? null;
        return new Account($id, $agent_id, $name, $login,$cellphone, $address, $status, $startdate, $enddate);

    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAgentId()
    {
        return $this->agent_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * @return mixed
     */
    public function getEnddate()
    {
        return $this->enddate;
    }


    public function toArray()
    {
        return [
            'id' => $this->id,
            'agent_id' => $this->agent_id,
            'name' => $this->name->toArray(),
            'login' => $this->login->toArray(),
            'cellphone' => $this->cellphone,
            'address' => $this->address->toArray(),
            'status' => $this->status,
            'startdate' => $this->startdate,
            'enddate' => $this->enddate
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }





}