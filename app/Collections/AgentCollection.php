<?php

namespace App\Collections;

use App\DomainModels\Agent;

class AgentCollection implements \Countable, \IteratorAggregate
{
    /** @var  Agents[] */
    private $agents;

    public function __construct(array $agents)
    {
        $this->agents = $agents;
    }

    public function getIterator(): AgentCollectionIterator
    {
        return new AgentCollectionIterator($this);
    }
    
    public function count(): int
    {
        return count($this->agents);
    }

    public function toArray(): array
    {
        return $this->agents;
    }

    /*
    * TODO : Not comfortable with this method
    * will do it later when have time
    */
    public function toAllArray(): array
    {
        $agentsJson = [];
        foreach ($this->getIterator() as $agent)
        {
            $agentsJson[] = $agent->toArray();
        }
        return $agentsJson;
    }

}