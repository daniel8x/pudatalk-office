<?php

namespace App\Collections;

use App\DomainModels\Account;

class AccountCollection implements \Countable, \IteratorAggregate
{
    /** @var  Accounts[] */
    private $accounts;

    public function __construct(array $accounts)
    {
        $this->accounts = $accounts;
    }

    public function getIterator(): AccountCollectionIterator
    {
        return new AccountCollectionIterator($this);
    }

    public function count(): int
    {
        return count($this->accounts);
    }

    public function toArray(): array
    {
        return $this->accounts;
    }

    /*
    * TODO : Not comfortable with this method
    * will do it later when have time
    */
    public function toAllArray(): array
    {
        $accountsJson = [];
        foreach ($this->getIterator() as $account)
        {
            $accountsJson[] = $account->toArray();
        }
        return $accountsJson;
    }

}