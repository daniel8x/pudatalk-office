<?php

namespace App\Collections;


use App\DomainModels\Account;

class AccountCollectionIterator implements \Iterator
{
    private $current = 0;

    /**
     * @var Account[]
     */
    private $accounts;

    public function __construct(AccountCollection $accountCollection)
    {
        $this->accounts = $accountCollection->toArray();
    }

    public function current(): Account
    {
        return $this->accounts[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->accounts[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }
}