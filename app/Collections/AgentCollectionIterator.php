<?php

namespace App\Collections;


use App\DomainModels\Agent;

class AgentCollectionIterator implements \Iterator
{
    private $current = 0;

    /**
     * @var Agent[]
     */
    private $agents;

    public function __construct(AgentCollection $agentCollection)
    {
        $this->agents = $agentCollection->toArray();
    }

    public function current(): Agent
    {
        return $this->agents[$this->current];
    }

    public function next(): void
    {
        $this->current++;
    }

    public function key(): int
    {
        return $this->current;
    }

    public function valid(): bool
    {
        return isset($this->agents[$this->current]);
    }

    public function rewind(): void
    {
        $this->current = 0;
    }
}