<?php

namespace App\Traits;

use Validator;

trait ValidationTrait
{

    private $errors;

    public function validator(array $requestData, array $rules): bool
    {
        if (empty($requestData))
            return false;
        $v = Validator::make($requestData, $rules);

        if ($v->fails()) {
            $this->errors = $v->messages();
            return false;
        }
        return true;
    }

    public function validationErrors()
    {
        return $this->errors;
    }

}
