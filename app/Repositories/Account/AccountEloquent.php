<?php
namespace App\Repositories\Account;

use App\Collections\AccountCollection;
use App\DomainModels\Account;
use App\EloquentModels\EAccount;
use App\DomainModels\Name;
use App\DomainModels\Login;
use App\DomainModels\Address;
use Illuminate\Database\Eloquent\Collection;

class AccountEloquent implements AccountInterface {
    protected $account;

    public function __construct(EAccount $account)
    {
        $this->account = $account;
    }

    public function getAll(): AccountCollection
    {
        $accountElCollection =  $this->account->all();
        return $this->accountDTOCollection($accountElCollection);
    }

    public function create(Account $account)
    {
        // Insert to Account table
         $this->account->create([
            'agent_id' => $account->getAgentId(),
            'firstname' => $account->getName()->getFirstName(),
            'middlename' => $account->getName()->getMiddleName(),
            'lastname' => $account->getName()->getLastName(),
            'email' => $account->getLogin()->getEmail(),
            'password' => bcrypt($account->getLogin()->getPassword()),
            'cellphone' => $account->getCellphone(),
            'address1' => $account->getAddress()->getAddress1(),
            'address2' => $account->getAddress()->getAddress2(),
            'city' => $account->getAddress()->getCity(),
            'state' => $account->getAddress()->getState(),
            'zipcode' => $account->getAddress()->getZipcode(),
            'status' => $account->getStatus(),
            'startdate' => $account->getStartdate(),
            'enddate' => $account->getEnddate()
        ]);

    }

    private function accountDTOCollection(Collection $data): AccountCollection
    {
        $accounts = [];
        $dataArray = $data->toArray();
        foreach ($dataArray as $each)
        {
            $accounts[] = Account::fromAssoc($this->makeAccountObject($each));
        }
        return new AccountCollection($accounts);

    }

    private function makeAccountObject(array $data): array{
        return ([
            'id' => $data['id'],
            'agent_id' => $data['agent_id'],
            'name' => Name::fromAssoc($data),
            'login' => Login::fromAssoc($data),
            'address' => Address::fromAssoc($data),
            'cellphone' => $data['cellphone'],
            'status' => $data['status'],
            'startdate' => $data['startdate'],
            'enddate' => $data['enddate'],

        ]);
    }

    public function getByAgentID($agentId): AccountCollection
    {
        $accountElCollection =  $this->account->find($agentId)->get();

        return $this->accountDTOCollection($accountElCollection);
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function getByID($id)
    {
        // TODO: Implement getByID() method.
    }

    public function update($id, array $data)
    {
        // TODO: Implement update() method.
    }
}