<?php
namespace App\Repositories\Account;

use App\DomainModels\Account;
use App\Collections\AccountCollection;

interface AccountInterface {
    public function getAll(): AccountCollection;
    public function create(Account $account);
    public function delete($id);
    public function getByID($id);
    public function getByAgentID($agentId): AccountCollection;
    public function update($id, array $data);
}