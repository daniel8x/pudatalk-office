<?php
namespace App\Repositories\Agent;

use App\Collections\AgentCollection;
use App\DomainModels\Agent;
use App\EloquentModels\EAgent;
use App\DomainModels\Name;
use App\DomainModels\Login;
use App\DomainModels\Address;
use Illuminate\Database\Eloquent\Collection;

class AgentEloquent implements AgentInterface {
    protected $agent;

    public function __construct(EAgent $agent)
    {
        $this->agent = $agent;
    }

    public function getAll(): AgentCollection
    {
        $agentElCollection =  $this->agent->all();
        return $this->agentDTOCollection($agentElCollection);
    }

    public function create(Agent $agent)
    {
        return $this->agent->create([
            'admin_id' => $agent->getAdminId(),
            'firstname' => $agent->getName()->getFirstName(),
            'lastname' => $agent->getName()->getLastName(),
            'email' => $agent->getLogin()->getEmail(),
            'password' => bcrypt($agent->getLogin()->getPassword()),
            'cellphone' => $agent->getCellphone(),
            'address1' => $agent->getAddress()->getAddress1(),
            'address2' => $agent->getAddress()->getAddress2(),
            'city' => $agent->getAddress()->getCity(),
            'state' => $agent->getAddress()->getState(),
            'zipcode' => $agent->getAddress()->getZipcode(),
            'status' => $agent->getStatus(),
            'ssn' => $agent->getSsn()
        ]);
    }

    private function agentDTOCollection(Collection $data): AgentCollection
    {
        $agents = [];
        $dataArray = $data->toArray();
        foreach ($dataArray as $each)
        {
            $agents[] = Agent::fromAssoc($this->makeAgentObject($each));
        }
        return new AgentCollection($agents);

    }

    private function makeAgentObject(array $data): array{
        return ([
            'id' => $data['id'],
            'admin_id' => $data['admin_id'],
            'name' => Name::fromAssoc($data),
            'login' => Login::fromAssoc($data),
            'address' => Address::fromAssoc($data),
            'cellphone' => $data['cellphone'],
            'ssn' => $data['ssn'],
            'status' => $data['status']
        ]);
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function getByID($id)
    {
        // TODO: Implement getByID() method.
    }

    public function update($id, array $data)
    {
        // TODO: Implement update() method.
    }
}