<?php
namespace App\Repositories\Agent;

use App\DomainModels\Agent;
use App\Collections\AgentCollection;

interface AgentInterface {
    public function getAll(): AgentCollection;
    public function create(Agent $agent);
    public function delete($id);
    public function getByID($id);
    public function update($id, array $data);
}