<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (env('LOADER') === 'ELOQUENT')
        {
            $this->app->bind('App\Repositories\Agent\AgentInterface', 'App\Repositories\Agent\AgentEloquent');
            $this->app->bind('App\Repositories\Account\AccountInterface', 'App\Repositories\Account\AccountEloquent');

        }
    }
}
