<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'id' => 1,
            'firstname' => 'Daniel',
            'lastname' => 'Trinh',
            'email' => 'danieltrinh.online@gmail.com',
            'password' => bcrypt('123456'),
        ]);

        DB::table('admins')->insert([
            'id' => 2,
            'firstname' => 'Phung',
            'lastname' => 'Pham',
            'email' => 'phung.pham@gmail.com',
            'password' => bcrypt('123456'),
        ]);

        DB::table('agents')->insert([
            'id' => 1,
            'admin_id' => 1,
            'firstname' => 'Agent',
            'lastname' => 'Agent',
            'email' => 'agent@gmail.com',
            'cellphone' => '36124932934',
            'address1' => '532 N.Campus',
            'address2' => '#423',
            'city' => 'Orlando',
            'state' => 'OH',
            'zipcode' => '422543',
            'status' => 1,
            'ssn' => '112-122-1242',
            'password' => bcrypt('123456'),
        ]);

        DB::table('agents')->insert([
            'id' => 2,
            'admin_id' => 2,
            'firstname' => 'Agent2',
            'lastname' => 'Agent2',
            'email' => 'agent2@gmail.com',
            'cellphone' => '36124932934',
            'address1' => '532 N.Campus',
            'address2' => '#423',
            'city' => 'Orlando',
            'state' => 'OH',
            'zipcode' => '422543',
            'status' => 1,
            'ssn' => '112-122-1242',
            'password' => bcrypt('123456'),
        ]);

        DB::table('accounts')->insert([
            'id' => 1,
            'agent_id' => 1,
            'firstname' => 'account f',
            'lastname' => 'account l',
            'email' => 'account@gmail.com',
            'username' => 'account',
            'password' => bcrypt('123456'),
            'cellphone' => '36124932934',
            'address1' => '532 N.Campus',
            'address2' => '#423',
            'city' => 'Orlando',
            'state' => 'OH',
            'zipcode' => '422543',
            'status' => 1,
            'startdate' => Carbon::create('2017', '01', '01'),
            'enddate' => Carbon::create('2018', '01', '01')
        ]);

        DB::table('roles')->insert([
            [
                'id' => 1,
                'name' => 'owner',
                'display_name' => 'Owner',
                'description' => 'Owner'
            ],
            [
                'id' => 2,
                'name' => 'manager',
                'display_name' => 'Manager',
                'description' => 'Manager'
            ],
            [
                'id' => 3,
                'name' => 'technician-khong-bao',
                'display_name' => 'Technician Khong Bao Luong',
                'description' => 'Technician Khong Bao Luong'
            ],
            [
                'id' => 4,
                'name' => 'technician-bao',
                'display_name' => 'Technician Bao Luong',
                'description' => 'Technician Bao Luong'
            ]
        ]);

        DB::table('salons')->insert([
            'id' => 1,
            'name' => 'salon 1',
            'account_id' => 1,
            'cellphone' => '23432234',
            'address1' => 'test',
            'address2' => 'test',
            'city' => 'test',
            'state' => 'OH',
            'zipcode' => '12345'
        ]);

        DB::table('employees')->insert([
            'id' => 1,
            'salon_id' => 1,
            'username' => 'daniel8x',
            'email' => 'danieltrinh.online@gmail.com',
            'password' => bcrypt('123456'),
            'pin' => '1234',
            'firstname' => 'Daniel',
            'middlename' => '',
            'lastname' => 'Trinh',
            'cellphone' => '36124932934',
            'address1' => '532 N.Campus',
            'address2' => '#423',
            'city' => 'Orlando',
            'state' => 'OH',
            'zipcode' => '422543',
            'status' => 1,
            'startdate' => Carbon::create('2017', '01', '01'),
            'enddate' => Carbon::create('2018', '01', '01')
        ]);

        DB::table('role_employee')->insert([
           'id' => 1,
           'employee_id' => 1,
           'role_id' => 1
        ]);
    }
}
