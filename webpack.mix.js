let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
    .extract([
        'jquery',
        'bootstrap'
    ])
    .autoload({
        jquery: ['$', 'window.jQuery', 'jQuery']
    })
    .sass('resources/assets/sass/style.scss', 'public/css/app.css')
    .version();


mix.browserSync({
    proxy: 'pudatalk-office.dev'
});