@extends('layouts.agent')

@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Register New Account</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="pills-struct mt-40">
                            <ul role="tablist" class="nav nav-pills nav-pills-rounded" id="myTabs_11">
                                <li class="active" role="presentation"><a aria-expanded="true" data-toggle="tab"
                                                                          role="tab" id="home_tab_11" href="#home_11">General
                                        Information</a></li>
                                <li role="presentation" class=""><a data-toggle="tab" id="profile_tab_11" role="tab"
                                                                    href="#profile_11" aria-expanded="false">Sign In
                                        Information</a></li>
                            </ul>
                            <div class="tab-content" id="myTabContent_11">
                                <div id="home_11" class="tab-pane fade active in" role="tabpanel">


                                    <div class="panel panel-default card-view">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-wrap">
                                                            <form action="#" class="form-horizontal">
                                                                <div class="form-body">
                                                                    <h6 class="txt-dark capitalize-font"><i
                                                                                class="zmdi zmdi-account mr-10"></i>Person's
                                                                        Info</h6>
                                                                    <hr class="light-grey-hr">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">First
                                                                                    Name</label>
                                                                                <div class="col-md-9">
                                                                                    <input type="text"
                                                                                           class="form-control"
                                                                                           placeholder="">
                                                                                    <span class="help-block"> </span>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Last
                                                                                    Name</label>
                                                                                <div class="col-md-9">
                                                                                    <input type="text"
                                                                                           class="form-control"
                                                                                           placeholder="">
                                                                                    <span class="help-block">  </span>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">SSN</label>
                                                                                <div class="col-md-9">
                                                                                    <input type="text" placeholder=""
                                                                                           data-mask="9999-99-9999"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Date
                                                                                    of Birth</label>
                                                                                <div class="col-md-9">
                                                                                    <input type="text" placeholder=""
                                                                                           data-mask="99/99/9999"
                                                                                           class="form-control">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->

                                                                    <div class="seprator-block"></div>

                                                                    <h6 class="txt-dark capitalize-font"><i
                                                                                class="zmdi zmdi-account-box mr-10"></i>address
                                                                    </h6>
                                                                    <hr class="light-grey-hr">
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Address
                                                                                    1</label>
                                                                                <div class="col-md-9">
                                                                                    <input type="text"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Address
                                                                                    2</label>
                                                                                <div class="col-md-9">
                                                                                    <input type="text"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">City</label>
                                                                                <div class="col-md-9">
                                                                                    <input type="text"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">State</label>
                                                                                <div class="col-md-9">
                                                                                    <select class="form-control">
                                                                                        <option value="AL">Alabama
                                                                                        </option>
                                                                                        <option value="AK">Alaska
                                                                                        </option>
                                                                                        <option value="AZ">Arizona
                                                                                        </option>
                                                                                        <option value="AR">Arkansas
                                                                                        </option>
                                                                                        <option value="CA">California
                                                                                        </option>
                                                                                        <option value="CO">Colorado
                                                                                        </option>
                                                                                        <option value="CT">Connecticut
                                                                                        </option>
                                                                                        <option value="DE">Delaware
                                                                                        </option>
                                                                                        <option value="DC">District Of
                                                                                            Columbia
                                                                                        </option>
                                                                                        <option value="FL">Florida
                                                                                        </option>
                                                                                        <option value="GA">Georgia
                                                                                        </option>
                                                                                        <option value="HI">Hawaii
                                                                                        </option>
                                                                                        <option value="ID">Idaho
                                                                                        </option>
                                                                                        <option value="IL">Illinois
                                                                                        </option>
                                                                                        <option value="IN">Indiana
                                                                                        </option>
                                                                                        <option value="IA">Iowa</option>
                                                                                        <option value="KS">Kansas
                                                                                        </option>
                                                                                        <option value="KY">Kentucky
                                                                                        </option>
                                                                                        <option value="LA">Louisiana
                                                                                        </option>
                                                                                        <option value="ME">Maine
                                                                                        </option>
                                                                                        <option value="MD">Maryland
                                                                                        </option>
                                                                                        <option value="MA">
                                                                                            Massachusetts
                                                                                        </option>
                                                                                        <option value="MI">Michigan
                                                                                        </option>
                                                                                        <option value="MN">Minnesota
                                                                                        </option>
                                                                                        <option value="MS">Mississippi
                                                                                        </option>
                                                                                        <option value="MO">Missouri
                                                                                        </option>
                                                                                        <option value="MT">Montana
                                                                                        </option>
                                                                                        <option value="NE">Nebraska
                                                                                        </option>
                                                                                        <option value="NV">Nevada
                                                                                        </option>
                                                                                        <option value="NH">New
                                                                                            Hampshire
                                                                                        </option>
                                                                                        <option value="NJ">New Jersey
                                                                                        </option>
                                                                                        <option value="NM">New Mexico
                                                                                        </option>
                                                                                        <option value="NY">New York
                                                                                        </option>
                                                                                        <option value="NC">North
                                                                                            Carolina
                                                                                        </option>
                                                                                        <option value="ND">North
                                                                                            Dakota
                                                                                        </option>
                                                                                        <option value="OH">Ohio</option>
                                                                                        <option value="OK">Oklahoma
                                                                                        </option>
                                                                                        <option value="OR">Oregon
                                                                                        </option>
                                                                                        <option value="PA">
                                                                                            Pennsylvania
                                                                                        </option>
                                                                                        <option value="RI">Rhode
                                                                                            Island
                                                                                        </option>
                                                                                        <option value="SC">South
                                                                                            Carolina
                                                                                        </option>
                                                                                        <option value="SD">South
                                                                                            Dakota
                                                                                        </option>
                                                                                        <option value="TN">Tennessee
                                                                                        </option>
                                                                                        <option value="TX">Texas
                                                                                        </option>
                                                                                        <option value="UT">Utah</option>
                                                                                        <option value="VT">Vermont
                                                                                        </option>
                                                                                        <option value="VA">Virginia
                                                                                        </option>
                                                                                        <option value="WA">Washington
                                                                                        </option>
                                                                                        <option value="WV">West
                                                                                            Virginia
                                                                                        </option>
                                                                                        <option value="WI">Wisconsin
                                                                                        </option>
                                                                                        <option value="WY">Wyoming
                                                                                        </option>
                                                                                    </select></div>

                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Zip
                                                                                    Code</label>

                                                                                <div class="col-md-9">
                                                                                    <input type="text"
                                                                                           class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">Country</label>
                                                                                <div class="col-md-9">
                                                                                    <select class="form-control">
                                                                                        <option selected>United State
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->
                                                                </div>
                                                                <div class="form-actions mt-10">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit"
                                                                                            class="btn btn-success  mr-10">
                                                                                        Submit
                                                                                    </button>
                                                                                    <button type="button"
                                                                                            class="btn btn-default">
                                                                                        Reset
                                                                                    </button>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6"></div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div id="profile_11" class="tab-pane fade" role="tabpanel">


                                    <div class="panel panel-default card-view">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-sm-4 col-xs-4">
                                                        <div class="form-wrap">
                                                            <form class="form-horizontal" method="POST"
                                                                  action="{{ route('agent.account.create') }}">
                                                                {{ csrf_field() }}
                                                                <div class="form-group @if ($errors->has('email')) has-error @endif">

                                                                    <label for="exampleInputEmail_3"
                                                                           class="col-sm-3 control-label">Email*</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon"><i
                                                                                        class="icon-envelope-open"></i>
                                                                            </div>
                                                                            <input type="email" class="form-control"
                                                                                   id="email" name="email"
                                                                                   placeholder="Enter email">
                                                                        </div>
                                                                        @if ($errors->has('email')) <span
                                                                                class="help-block">{{ $errors->first('email') }}</span> @endif

                                                                    </div>

                                                                </div>

                                                                <div class="form-group @if ($errors->has('password')) has-error @endif">

                                                                    <label for="exampleInputpwd_32"
                                                                           class="col-sm-3 control-label">Password*</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon"><i
                                                                                        class="icon-lock"></i></div>
                                                                            <input type="password" class="form-control"
                                                                                   id="password" name="password"
                                                                                   placeholder="Enter pwd">
                                                                        </div>
                                                                        @if ($errors->has('password')) <span
                                                                                class="help-block">{{ $errors->first('password') }}</span> @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group @if ($errors->has('password_confirm')) has-error @endif">

                                                                    <label for="exampleInputpwd_4"
                                                                           class="col-sm-3 control-label">Re
                                                                        Password*</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <div class="input-group-addon"><i
                                                                                        class="icon-lock"></i></div>
                                                                            <input type="password" class="form-control"
                                                                                   id="password_confirm"
                                                                                   name="password_confirm"
                                                                                   placeholder="Re Enter pwd">
                                                                        </div>
                                                                        @if ($errors->has('password_confirm')) <span
                                                                                class="help-block">{{ $errors->first('password_confirm') }}</span> @endif

                                                                    </div>
                                                                </div>

                                                                <div class="form-group mb-0">
                                                                    <div class="col-sm-offset-3 col-sm-9">
                                                                        <button type="submit"
                                                                                class="btn btn-success mr-10">Submit
                                                                        </button>
                                                                        <button type="submit" class="btn btn-default">
                                                                            Cancel
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
@endsection