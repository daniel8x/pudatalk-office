@extends('layouts.agent')

@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        @if($numberOfAccount == 0)
                            There is no accounts
                        @else
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="example" class="table table-hover display  pb-30" >
                                        <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Username</th>
                                            <th>Cellphone</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>

                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Username</th>
                                            <th>Cellphone</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>

                                        @foreach($accounts as $account)
                                            <tr>
                                                <td>{{$account['name']['firstname']}}</td>
                                                <td>{{$account['name']['lastname']}}</td>
                                                <td>{{$account['login']['email']}}</td>
                                                <td>{{$account['login']['username']}}</td>
                                                <td>{{$account['cellphone']}}</td>
                                                <td>{{$account['startdate']}}</td>
                                                <td>{{$account['enddate']}}</td>
                                                <td>{{$account['status']}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection