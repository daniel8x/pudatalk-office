<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span>Main</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a class="active" href="/admin"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">dashboard</span></div><div class="clearfix"></div></a>
				</li>
				
				
				
				<li><hr class="light-grey-hr mb-10"/></li>
				<li class="navigation-header">
					<span>Agents</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a href="{{url('/admin')}}/agent/create" id="create-new-agent" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">New Agent</span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="{{url('/admin')}}/agent" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Management </span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Reports </span></div><div class="clearfix"></div></a>
				</li>
				
				<li><hr class="light-grey-hr mb-10"/></li>
				<li class="navigation-header">
					<span>Sale</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Analyst </span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Reports </span></div><div class="clearfix"></div></a>
				</li>
				<li><hr class="light-grey-hr mb-10"/></li>
				<li class="navigation-header">
					<span>Business Intelligence</span> 
					<i class="zmdi zmdi-more"></i>
				</li>
				<li>
					<a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Quaterly Chart </span></div><div class="clearfix"></div></a>
				</li>
				<li>
					<a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Anualy Chart </span></div><div class="clearfix"></div></a>
				</li>
			
			</ul>
		</div>