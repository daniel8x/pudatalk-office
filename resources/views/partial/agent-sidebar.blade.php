<div class="fixed-sidebar-left">
<ul class="nav navbar-nav side-nav nicescroll-bar">
    <li class="navigation-header">
        <span>Main</span>
        <i class="zmdi zmdi-more"></i>
    </li>
    <li>
        <a class="active" href="/agent"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">dashboard</span></div><div class="clearfix"></div></a>
    </li>

    <li><hr class="light-grey-hr mb-10"/></li>
    <li class="navigation-header">
        <span>Accounts</span>
        <i class="zmdi zmdi-more"></i>
    </li>
    <li>
        <a href="{{url('/agent')}}/account/create" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">New Account</span></div><div class="clearfix"></div></a>
    </li>
    <li>
        <a href="{{url('/agent')}}/account" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Management </span></div><div class="clearfix"></div></a>
    </li>
    <li>
        <a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Reports </span></div><div class="clearfix"></div></a>
    </li>

    <li><hr class="light-grey-hr mb-10"/></li>
    <li class="navigation-header">
        <span>Sale</span>
        <i class="zmdi zmdi-more"></i>
    </li>
    <li>
        <a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Analyst </span></div><div class="clearfix"></div></a>
    </li>
    <li>
        <a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Reports </span></div><div class="clearfix"></div></a>
    </li>
    <li><hr class="light-grey-hr mb-10"/></li>
    <li class="navigation-header">
        <span>Business Intelligence</span>
        <i class="zmdi zmdi-more"></i>
    </li>
    <li>
        <a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Quaterly Chart </span></div><div class="clearfix"></div></a>
    </li>
    <li>
        <a href="javascript:void(0);" ><div class="pull-left"><i class="zmdi zmdi-smartphone-setup mr-20"></i><span class="right-nav-text">Anualy Chart </span></div><div class="clearfix"></div></a>
    </li>

</ul>
</div>