<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Pudatalk - Administrator Login</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="pudatalk"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--<link rel="shortcut icon" href="favicon.ico">--}}
    {{--<link rel="icon" href="favicon.ico" type="image/x-icon">--}}
    
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css"/>

</head>

<body>
<!--Preloader-->
{{-- <div class="preloader-it">
    <div class="la-anim-1"></div>
</div> --}}
<!--/Preloader-->

<div class="wrapper pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="index.html">
                <img class="brand-img mr-10" src="dist/img/logo.png" />
                <span class="brand-text">Pudatalk</span>
            </a>
        </div>
        <div class="form-group mb-0 pull-right">
            <span class="inline-block pr-10">Don't have an account?</span>
            <a class="inline-block btn btn-info btn-success btn-rounded btn-outline" href="signup.html">Sign Up</a>
        </div>
        <div class="clearfix"></div>
    </header>
    
    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 auth-page">
        <div class="container-fluid">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h3 class="text-center txt-dark mb-10">Sign in Administrator </h3>
                                    <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                                </div>	
                                <div class="form-wrap">
                                    <form method="POST" action="" novalidate>
                                        {{ csrf_field() }}
                                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                                            <label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
                                            <input type="email" name="email" class="form-control" value="{{ old('email') }}" id="email" placeholder="Enter email">
                                                @if ($errors->has('email')) <span class="help-block">{{ $errors->first('email') }}</span> @endif
                                        </div>
                                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                                            <label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password</label>
                                            <a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="#">forgot password ?</a>
                                            <div class="clearfix"></div>
                                            <input type="password" name="password" class="form-control" required id="password" placeholder="Enter pwd">
                                                @if ($errors->has('password')) <span class="help-block">{{ $errors->first('password') }}</span> @endif

                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="checkbox checkbox-primary pr-10 pull-left">
                                                <input id="checkbox_2" type="checkbox">
                                                <label for="checkbox_2"> Keep me logged in</label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group text-center">
                                            <input type="submit" class="btn btn-info btn-success btn-rounded" id="btn_signin" value="Sign In"/>
                                        </div>
                                    </form>
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->	
        </div>
        
    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
</body>

</html>
