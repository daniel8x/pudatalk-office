@extends('layouts.admin')

@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        @if($numberOfAgent == 0)
                            There is no agents
                        @else
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="example" class="table table-hover display  pb-30" >
                                        <thead>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Cellphone</th>
                                            <th>SSN</th>
                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Cellphone</th>
                                            <th>SSN</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>

                                        @foreach($agents as $agent)
                                        <tr>
                                            <td>{{$agent['name']['firstname']}}</td>
                                            <td>{{$agent['name']['lastname']}}</td>
                                            <td>{{$agent['login']['email']}}</td>
                                            <td>{{$agent['cellphone']}}</td>
                                            <td>{{$agent['ssn']}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

@endsection